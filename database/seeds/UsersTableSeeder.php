<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
		DB::table('roles')->truncate();
		DB::table('role_users')->truncate();

		$role = [
			'name' => 'Administrator',
			'slug' => 'administrator',
			'permissions' => [
				'admin' => true,
				'vendedor' => true,
			]
		];
		$vendedor = [
			'name' => 'Vendedor',
			'slug' => 'vendedor',
			'permissions' => [
				'vendedor' => true,
			]
		];

		$adminRole = Sentinel::getRoleRepository()->createModel()->fill($role)->save();
		$vendedorRole = Sentinel::getRoleRepository()->createModel()->fill($vendedor)->save();

		
		$admin = [
			'email'    	=> 'admin@example.com',
			'first_name'    => 'admin',
			'last_name'    	=> 'admin',
			'password' 	=> '123456',
		];

		$users = [

			[
				'email'    	=> 'vendedor1@example.com',
				'first_name'    => 'vendedor1',
				'last_name'    	=> 'vendedor1',
				'password' 	=> '123456',
			],

			[
				'email'    	=> 'vendedor2@example.com',
				'first_name'    => 'vendedor2',
				'last_name'    	=> 'vendedor2',
				'password' 	=> '123456',
			],

			[
				'email'    	=> 'vendedor3@example.com',
				'first_name'    => 'vendedor3',
				'last_name'    	=> 'vendedor3',
				'password' 	=> '123456',
			],

		];

		$adminUser = Sentinel::registerAndActivate($admin);
		$adminUser->roles()->attach($adminRole);

		foreach ($users as $user)
		{
			$vendedor=Sentinel::registerAndActivate($user);
		}
    }
}
