<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Session;
use Request;
use Arr;
class SentinelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(func_get_args());
        $role = Arr::except(func_get_args(), [0,1]);
        if(!Sentinel::check()){
            Session::flash('error', 'Usted debe estar conectado!.');
            return redirect('/');
        }
        if (!empty($role)){
            if (!Sentinel::hasAnyAccess($role)){
                Session::flash('error', 'Tu Perfil, no puede realizar este tipo de acciones.');
                   return redirect()->route('dashboard');
               
           }
       }
       return $next($request);
   }
}
