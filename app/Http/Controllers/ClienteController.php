<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Validator;
use Session;
use Redirect;
use URL;
use Carbon\Carbon;
use DB;
#DB
use App\Client;

class ClienteController extends Controller
{
    private $rules = [
        'nombre'    => 'required|min:3',
        'documento' => 'required|min:3',
        'email'     => 'required|email'
    ];
    private $messages = [
        '*.required'  => 'Campo obligatorio.',
        '*.min'       => 'Este campo debe ser mayor de :min caracteres.',
        'email.email' => 'Ingrese email valido',        
    ];
    /**
    * Listar todos los clientes de la BD
    */
    public function index(){
        try{
            $clients=Client::paginate(10);
            $search='';
            return view('clientes.index',compact('clients','search'));
        }catch(\Exception $ex){
            Session::flash('error', 'Ha surgido un error');
            return Redirect::to(URL::previous());
        }
    }

    /**
    * Función para crear usuarios(Muestra la vista)
    */
    public function create(){
        try{
            return View("clientes.register");
        }catch(\Exception $ex){
            Session::flash('error', 'Ha surgido un error');
            return Redirect::to(URL::previous());
        }
    }
    /**
    * Función para registrar usuario
    */
    public function store(Request $request){
        $validator = Validator::make($request->all(), $this->rules, $this->messages);

        if ($validator->fails()) {
            return Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }
        $cliente            =new Client();
        $cliente->email     =$request->input('email');
        $cliente->nombre    =$request->input('nombre');
        $cliente->documento =$request->input('documento');
        $cliente->direccion =$request->input('direccion');
        $cliente->save();
        if($cliente){
            Session::flash('success', 'Cliente Creado con Éxito!.');
            return Redirect::route('clients.index');
        }else{
            return Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $cliente     =Client::find($id);
            if(!$cliente ){
                return Redirect::to(URL::previous())->withInput();
            }
            return view('clientes.register',compact('cliente'));
        }catch(Exception $ex){
            Session::flash('error', 'Ha surgido un error');
            return Redirect::to(URL::previous());
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
             $cliente     =Client::find($id);
            if(!$cliente ){
                Session::flash('error', 'No existe cliente');
                return Redirect::to(URL::previous())->withInput();
            }
            $validator = Validator::make($request->all(), $this->rules, $this->messages);
            if ($validator->fails()) {
                return Redirect::to(URL::previous())->withInput()->withErrors($validator);
            }
            $cliente->email     =$request->input('email');
            $cliente->nombre    =$request->input('nombre');
            $cliente->documento =$request->input('documento');
            $cliente->direccion =$request->input('direccion');
            $cliente->save();
            Session::flash('success', 'Cliente actualizado con Éxito!.');
            return Redirect::route('clients.index');
        }catch(Exception $ex){
            Session::flash('error', 'Ha surgido un error');
            return Redirect::to(URL::previous());
        }
    }
    /*
    *Eliminar cliente
    * @param  int  $id
    * @return \Illuminate\Http\Response
    **/

    public function destroy($id){
        if(!DB::table('clients')->where('id', $id)->delete()){
            Session::flash('error', 'Cliente no encontrado');
            return Redirect::route('clients.index');
        }
        Session::flash('success', 'Cliente eliminado');
        return Redirect::route('clients.index');
    }

    public function search(Request $request)
    {
        $search  =$request->input('search');
        $clients = Client::where('email','like',"%". $search."%")->orWhere('nombre','like',"%". $search."%")->orWhere('documento','like',"%". $search."%")->orWhere('direccion','like',"%". $search."%")->paginate(10);

        return view('clientes.index',compact('clients','search'));
    }
}
