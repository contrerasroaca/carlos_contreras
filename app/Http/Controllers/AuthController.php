<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Validator;
use Session;
use Redirect;
use URL;
use \Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use \Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Illuminate\Support\Facades\Hash;
use \Cartalyst\Sentinel\Roles;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Reminder;
use Carbon\Carbon;
#DB
use App\User;

class AuthController extends Controller
{
    private $rules = [
        'first_name'       => 'required|min:3',
        'last_name'        => 'required|min:3',
        'email'            => 'required|email|unique:users',
        'password'         => 'required|between:8,32',
        'password_confirm' => 'required|same:password',
        'role_id'          => 'required',
    ];
    private $rules_login = [
        'email'    => 'required|email',
        'password' => 'required|between:3,32'
    ];
    private $messages = [
        'email.required'        => 'Ingresa un correo electrónico.',
        'password.required'     => 'Debes ingresar una clave valida.',
        '*.min'                 => 'Este campo debe ser mayor de :min caracteres.',
        'email.unique'          => 'El email ya existe!',
        'email.email'           => 'Ingrese email valido',
        'password.between'      => 'La contraseña debe tener entre :min y :max',
        'password_confirm.same' => 'Las contraseñas deben coincidir',
    ];
    /**
    * Listar todos los usuarios de la BD
    */
    public function index(){
        try{
            $users=User::paginate(10);

            $search='';
            return view('usuarios.index',compact('users','search'));
        }catch(\Exception $ex){
            Session::flash('error', 'Ha surgido un error');
            return Redirect::to(URL::previous());
        }
    }
    /**
    * Función para el logueo
    */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules_login, $this->messages);
        if ($validator->fails()) 
        {
            return back()->withInput()->withErrors($validator);
        }
        try {
            if (Sentinel::authenticate($request->only('email', 'password')))
            {
                //return View("welcome");
                return redirect()->route('dashboard');
            }

            $message='Correo o Contraseña no validos';
        } catch (NotActivatedException $e) {
            $message='Cuenta no activa';
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            $message='Cuenta Suspendida por '.$delay.' Segundos';
        }
        Session::flash('error', $message);
        return back()->withInput()->withErrors($message);
    }
    /**
    * Función para cerrar session
    */
    public function logout(Request $request){
        try {
            if (!empty(Sentinel::getUser())) {
                Sentinel::logout();
            } 
            return Redirect::to('/');

        } catch (Exception $ex) {
            return Redirect::to('/');
        }
    }
    /**
    * Función para crear usuarios(Muestra la vista)
    */
    public function create(){
        try{
            $roles    = Sentinel::getRoleRepository()->get()->pluck('name','id');
            return View("usuarios.register",compact('roles'));
        }catch(\Exception $ex){
            Session::flash('error', 'Ha surgido un error');
            return Redirect::to(URL::previous());
        }
    }
    /**
    * Función para registrar usuario
    */
    public function store(Request $request){
        $validator = Validator::make($request->all(), $this->rules, $this->messages);
        if ($validator->fails()) {
            return Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }
        $activate=$request->input('activate')? true:false;
        $user = Sentinel::register(array(
            'first_name' => $request->input('first_name'),
            'last_name'  => $request->input('last_name'),
            'email'      => $request->input('email'),
            'password'   => $request->input('password')
        ), $activate);
        if($user){
            $role = Sentinel::findRoleById($request->input('role_id'));
            $role->users()->attach($user);
            Session::flash('success', 'Usuario Creado con Éxito!.');
            return Redirect::route('users.index');
        }else{
            return Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{

            $roles    = Sentinel::getRoleRepository()->get()->pluck('name','id');
            $user     =User::find($id);
            if(!$user){
                return Redirect::to(URL::previous())->withInput();
            }
            return view('usuarios.register',compact('roles','user'));
        }catch(Exception $ex){
            Session::flash('error', 'Ha surgido un error');
            return Redirect::to(URL::previous());
        }
    }
    /**
    * Activar usuario
    */
    /**
     * Activa the specified usuario.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id, $estado) {
        try {
            $user = Sentinel::findById($id);
        } catch (UserNotFoundException $e) {
            Session::flash('error', 'Usuario no encontrado!');
            return Redirect::route('users.index');
        }
        $status = Activation::completed($user);
        if ($estado == 1) {
            $mensaje='Activado';
            if ($estado != $status) {
                $activation = Activation::exists($user);
                if ($activation) {
                    Activation::remove($user);
                }
                $user_activation=Activation::create($user);
                Activation::complete($user, $user_activation->code);
            }
        } else {
            $mensaje='Desactivado';
            Activation::remove($user);
        }
        Session::flash('success', 'Usuario&nbsp;<strong>'.$mensaje.'</strong>&nbsp;con Éxito!');        
        return Redirect::route('users.index');
    }
     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
        try{
            $user     =User::find($id);
            if(!$user){
                return Redirect::to(URL::previous())->withInput()->with('warning', 'Usuario No existe');
            }
            /**
            * Excluimos el email actual de la validacion
            */
            $this->rules['email']=$this->rules['email'].',email,'.$user->id;
            /**
            * Si no se va a actualizar el password
            */
            if (!$password = $request->input('password')) {
                unset($this->rules['password']);
                unset($this->rules['password_confirm']);
            }            
            $validator = Validator::make($request->all(), $this->rules, $this->messages);
            if ($validator->fails()) {
                return Redirect::to(URL::previous())->withInput()->withErrors($validator);
            }

            $activate=$request->input('activate')? true:false;
            /**
            * Actualizar Usuario
            */
            $user->first_name = $request->input('first_name');
            $user->last_name  = $request->input('last_name');
            $user->email      = $request->input('email');

            if ($password) {
                $user->password = Hash::make($password);
            }
            /**
            * ASignamos nuevos roles al usuario
            */
            $userRoles = $user->roles()->pluck('id')->toArray();
            if(!is_array($userRoles)){
                $userRoles=array(0=>"");
            }
            // Obtenes el  rol seleccioando del usario

            $selectedRoles = array(0=>$request->input('role_id'));
            /**
            * Comparación de grupos entre los grupos que el usuario actualmente
            */
            /**
            * have y los grupos que el usuario desea tener.
            */
            $rolesToAdd    = array_diff($selectedRoles, $userRoles);
            $rolesToRemove = array_diff($userRoles, $selectedRoles);

            /**
            * Asignar al usuario a grupos
            */
            foreach ($rolesToAdd as $roleId) {
                $role = Sentinel::findRoleById($roleId);

                $role->users()->attach($user);
            }

            /**
            * Eliminar al usuario de los grupos
            */
            foreach ($rolesToRemove as $roleId) {
                $role = Sentinel::findRoleById($roleId);

                $role->users()->detach($user);
            }
            /**
            * Activar/Desactivar Usuario
            */
            $status = $activation = Activation::completed($user);
            if ($request->input('activate') != $status) {
                if ($request->input('activate')) {
                    $activation = Activation::exists($user);
                    if ($activation) {
                        Activation::remove($user);
                    }
                    $user_activation=Activation::create($user);
                    Activation::complete($user, $user_activation->code);
                } else {
                    Activation::remove($user);
                }
            }
            $user->save();
            Session::flash('success', '&nbsp;Usuario&nbsp;<strong>'. $user->email.'</strong>&nbsp;Actualizado&nbsp;con&nbsp;Éxito!');
            return Redirect::route('users.index');
        }catch(Exception $ex){
            Session::flash('error', '&nbsp;Ha srugido un inconveniente');
            return Redirect::route('users.index');
        }
    }
    
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try{
            $user     =User::find($id);
            if(!$user){
                return Redirect::to(URL::previous())->withInput();
            }
            return view('usuarios.edit_profile',compact('user'));
        }catch(Exception $ex){
            Session::flash('error', 'Ha surgido un error');
            return Redirect::to(URL::previous());
        }
    }
    /**
     * Update_user the specified resource in storage.
     * se actualiza el perfil del usuario por el  propio usuario
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_user(Request $request, $id)
    {
        try{
            $user     =User::find($id);
            if(!$user){
                Session::flash('warning', 'Usuario No existe');
                return Redirect::to(URL::previous())->withInput();
            }
            /*Excluimos las reglas de validacion que el usuario no puede actualizar*/
            unset($this->rules['cliente_id']);
            unset($this->rules['sede_id']);
            unset($this->rules['role_id']);
            /**
            * Excluimos el email actual de la validacion
            */
            $this->rules['email']=$this->rules['email'].',email,'.$user->id;
            /**
            * Si no se va a actualizar el password
            */
            if (!$password = $request->input('password')) {
                unset($this->rules['password']);
                unset($this->rules['password_confirm']);
            }            
            $validator = Validator::make($request->all(), $this->rules, $this->messages);
            if ($validator->fails()) {
                return Redirect::to(URL::previous())->withInput()->withErrors($validator);
            }
            /**
            * Actualizar Usuario
            */
            $user->first_name = $request->input('first_name');
            $user->last_name  = $request->input('last_name');
            $user->email      = $request->input('email');

            if ($password) {
                $user->password = Hash::make($password);
            }
            
            $user->save();
            Session::flash('success', '&nbsp;Usuario&nbsp;<strong>'. $user->email.'</strong>&nbsp;Actualizado&nbsp;con&nbsp;Éxito!');
                     // Redirect to the user page
            return Redirect::route('dashboard');
        }catch(Exception $ex){
            Session::flash('error', 'Ha srugido un inconveniente');
            return Redirect::route('users.index');
        }
    }
    public function search(Request $request)
    {
        $search=$request->input('search');
        $users     = User::where('email','like',"%". $search."%")->orWhere('first_name','like',"%". $search."%")->orWhere('last_name','like',"%". $search."%")->orWhere('created_at','like',"%". $search."%")->orwherehas('roles', 
            function ($query) use ($search) {
                $query->Where('name', 'like',"%". $search."%");
            })->paginate(10);

        return view('usuarios.index',compact('users','search'));
    }
    public function destroy($id){
        $user = User::find($id);
        if(!$user){
            Session::flash('error', 'Usuario no encontrado');
            return Redirect::route('users.index');
        }
        $user->delete();
        Session::flash('success', 'Usuario eliminado');
        return Redirect::route('users.index');
    }
}
