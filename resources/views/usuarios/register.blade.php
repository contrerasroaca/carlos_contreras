@extends('adminlte::page')
@section('content')
<!--Begin::Row-->
<div class="row">
	<div class="col-xl-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Editar Usuario</h3>
            <a href="{{ route('users.index') }}" class="btn btn-success float-right">
                Usuarios &nbsp;
                <i class="fa fa-list-alt"></i>
            </a>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @if(empty($user))
        <form class="kt-form" action="{{ route('users.store') }}" method="POST">
            @else
            <form method="POST" action="{{ route('users.update',$user) }}">
                <input name="_method" type="hidden" value="PUT">
                @endif
                @csrf
                <div class="card-body">
                   @include('flash_message')
                   <div class="form-group">
                    <label class="">Correo Electrónico</label>
                    <input type="email" class="form-control" aria-describedby="emailHelp" autocomplete="off" placeholder="Ingrese correo electrónico" name="email" id="email" value="{{ empty($user)?'':$user->email }}">
                    <span class="form-text text-muted">Este correo no puede repetirse.</span>
                    @error('email')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="">Nombres</label>
                    <input type="text" class="form-control" aria-describedby="first_nameHelp"  autocomplete="off" placeholder="Ingrese Nombres" name="first_name" id="first_name" value="{{ empty($user)?'':$user->first_name }}">
                    <span class="form-text text-muted">Nombre del usuario.</span>
                    @error('first_name')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="">Apellidos</label>
                    <input type="text" class="form-control" aria-describedby="last_nameHelp"  autocomplete="off" placeholder="Ingrese Apellidos" name="last_name" id="last_name" value="{{ empty($user)?'':$user->last_name }}">
                    <span class="form-text text-muted">Apellidos del usuario.</span>
                    @error('last_name')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                @if(!empty($user))
                <div class="form-group row">
                    <div class="col-md-12 btn btn-bold btn-sm btn-font-sm  btn-label-warning"><i class="fa fa-info-circle"></i>&nbsp;Si no Desea cambiar contraseña deje en blanco</div>
                </div>
                @endif
                <div class="form-group">
                    <label class="">Clave</label>
                    <input type="password" class="form-control" id="password" name="password" autocomplete="off" placeholder="Clave">
                    <span class="form-text text-muted">Para mayor seguridad, ingrese caracteres especiales, caracteres especiales y numeros.</span>
                    @error('password')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="">Confirmación de Clave</label>
                    <input type="password" class="form-control" id="password_confirm" name="password_confirm" autocomplete="off" placeholder="Confirmación de Clave">
                    <span class="form-text text-muted">Debe coincidir con al clave.</span>
                    @error('password_confirm')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="">Rol</label>
                    <select class="form-control select2" id="role_id" name="role_id">
                        <option value="">Seleccione</option>
                        @foreach ($roles as $key => $value)
                        @php 
                        $selected='';
                        @endphp
                        @if(!empty($user)&&$user->role_id==$key)
                        @php 
                        $selected='selected';
                        @endphp
                        @endif
                        <option {{ $selected }} value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>
                    @error('role_id')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="">Activo</label><br>
                    <span class="">
                        <label>
                            <input type="checkbox" name="activate" id="activate" {{(isset($user)&&Activation::completed($user))?'checked="checked"':''}}>
                            <span></span>
                        </label>
                    </span>
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <span class="kt-margin-left-10">o <a href="{{ route('users.index') }}" class="kt-link kt-font-bold">Cancelar</a></span>
              </div>
          </div>
      </form>
  </div>
</div>
<!--End::Row-->
@endsection
@push('js')
<script type="text/javascript">
  $(document).ready(function() {
   $('.select2').select2({width:"100%"});
   @if(!empty($user))
   @if(!empty($user->getRoles()->first()->id))
   $('#role_id').val("{{$user->getRoles()->first()->id}}").trigger('change');
   @endif
   @endif
});
</script>
@endpush