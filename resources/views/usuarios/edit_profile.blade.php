@extends('adminlte::page')
@section('content')
@include('edit')
<div class="row">
	<div class="col-xl-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Editar Usuario</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="kt-form" action="{{ route('usersupdate',$user) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
              @include('flash_message')
              <div class="form-group">
                <label class="kt-font-boldest">Correo Electrónico</label>
                <input type="email" class="form-control" aria-describedby="emailHelp" autocomplete="off" placeholder="Ingrese correo electrónico" name="email" id="email" value="{{ empty($user)?'':$user->email }}" readonly="readonly">
                <span class="form-text text-muted">Este correo no puede repetirse.</span>
                @error('email')
                <span class="text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label class="kt-font-boldest">Nombres</label>
                <input type="text" class="form-control" aria-describedby="first_nameHelp"  autocomplete="off" placeholder="Ingrese Nombres" name="first_name" id="first_name" value="{{ empty($user)?'':$user->first_name }}">
                <span class="form-text text-muted">Nombre del usuario.</span>
                @error('first_name')
                <span class="text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label class="kt-font-boldest">Apellidos</label>
                <input type="text" class="form-control" aria-describedby="last_nameHelp"  autocomplete="off" placeholder="Ingrese Apellidos" name="last_name" id="last_name" value="{{ empty($user)?'':$user->last_name }}">
                <span class="form-text text-muted">Apellidos del usuario.</span>
                @error('last_name')
                <span class="text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            @if(!empty($user))
            <div class="form-group row">
                <div class="col-md-12 btn btn-bold btn-sm btn-font-sm  btn-label-warning"><i class="fa fa-info-circle"></i>&nbsp;Si no Desea cambiar contraseña deje en blanco</div>
            </div>
            @endif
            <div class="form-group">
                <label class="kt-font-boldest">Clave</label>
                <input type="password" class="form-control" id="password" name="password" autocomplete="off" placeholder="Clave">
                <span class="form-text text-muted">Para mayor seguridad, ingrese caracteres especiales, caracteres especiales y numeros.</span>
                @error('password')
                <span class="text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label class="kt-font-boldest">Confirmación de Clave</label>
                <input type="password" class="form-control" id="password_confirm" name="password_confirm" autocomplete="off" placeholder="Confirmación de Clave">
                <span class="form-text text-muted">Debe coincidir con al clave.</span>
                @error('password_confirm')
                <span class="text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
          <span class="">o <a href="{{ route('users.index') }}" class="">Cancelar</a></span>
      </div>
  </form>
</div>
</div>
</div>
@endsection
