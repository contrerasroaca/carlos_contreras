@extends('adminlte::page')
@section('content')
@include('edit')
<!--Begin::Row-->
<div class="row">
	<div class="col-xl-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Usuarios</h3>
            <a href="{{ route('users.create') }}" class="btn btn-success float-right">
                Nuevo Usuario &nbsp;
                <i class="fa fa-user"></i>
            </a>
        </div>

        <div class="card-body">
            @include('flash_message')
            <table class="table table-striped table-compact" width="100%">
                <thead>
                    <tr class=" text-right">
                        <td colspan="4">
                            <form action="{{route('userssearch')}}" method="GET">
                                <div class="input-group input-group-md">
                                    <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{($search)?$search:''}}">
                                    <span class="input-group-btn">
                                        <div class="btn-group">
                                            <button class="btn btn-success" type="submit"><i class="fa fa-search text-white"></i>Buscar!</button>
                                            <a href="{{route('users.index')}}" class="btn btn-primary"><i class="fa fa-broom text-white"></i>Limpiar</a>
                                        </div>
                                    </span>
                                </div>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <th>Nombres</th>
                        <th>Email</th>
                        <th>Rol</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($users as $key => $value)
                    <tr>
                        <td>{!! $value->name !!}</td>
                        <td>{!! $value->email !!}</td>
                        <td>{!! (count($value->getRoles())>0)?$value->getRoles()->first()->name : '' !!}</td>
                        <td>
                            <div class="btn-group" role="group" aria-label="First group">
                                <a href="{{route('users.edit', $value)}}" class="btn btn-warning"  title="" ><i class="fa fa-edit"></i></a>
                                @if(Activation::completed($value))
                                <a href="{{route('usersactivate',[$value->id,0])}}" title="" class="btn btn-danger"><i class="fa fa-power-off"></i></a>
                                @else
                                <a href ="{{route('usersactivate',[$value->id,1])}}" class="btn btn-dark" title="" ><i class="fa fa-power-off"></i></a>
                                @endif
                                <form method="POST" action="{{ route('users.destroy',$value) }}">
                                     @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button type="submit" class="btn btn-info"><i class="fa fa-trash"></i></button>
                                </form>
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr class=" text-center"><td colspan="4"><i class="fa fa-times-circle"></i>&nbsp;No se Encontraron resultados</td></tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="row   d-block mx-auto text-center ">
                    @if($users)
                    <div class="col-md-12 d-block mx-auto text-center centerpagination">
                        @if(empty($search))
                        {{ $users->appends([])->links() }}
                        @else
                        {{ $users->appends(['search'=>$search])->links() }}
                        @endif
                        @if($users->total()>0)
                        &nbsp;Registros&nbsp;<strong>{{$users->count()}}</strong>&nbsp;de&nbsp;<strong>{{$users->total()}}</strong>&nbsp;-&nbsp;Página&nbsp;<strong>{{$users->currentPage()}}</strong>&nbsp;de&nbsp;<strong>{{$users->lastPage()}}</strong>
                    </div>
                    @endif
                    @endif
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
        </div>
    </div>
</div>
<!--End::Row-->
@endsection
