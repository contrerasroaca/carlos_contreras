@extends('adminlte::page')
@section('content')
@include('edit')
<!--Begin::Row-->
<div class="row">
	<div class="col-xl-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">CLientes</h3>
            <a href="{{ route('clients.create') }}" class="btn btn-success float-right">
                Nuevo CLiente &nbsp;
                <i class="fa fa-user"></i>
            </a>
        </div>

        <div class="card-body">
            @include('flash_message')
            <table class="table table-striped table-compact" width="100%">
                <thead>
                    <tr class=" text-right">
                        <td colspan="5">
                            <form action="{{route('clientssearch')}}" method="GET">
                                <div class="input-group input-group-md">
                                    <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{($search)?$search:''}}">
                                    <span class="input-group-btn">
                                        <div class="btn-group">
                                            <button class="btn btn-success" type="submit"><i class="fa fa-search text-white"></i>Buscar!</button>
                                            <a href="{{route('clients.index')}}" class="btn btn-primary"><i class="fa fa-broom text-white"></i>Limpiar</a>
                                        </div>
                                    </span>
                                </div>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <th>Correo</th>
                        <th>Nombre</th>
                        <th>Documento</th>
                        <th>Dirección</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($clients as $key => $value)
                    <tr>
                        <td>{!! $value->email !!}</td>
                        <td>{!! $value->nombre !!}</td>
                        <td>{!! $value->documento !!}</td>
                        <td>{!! $value->direccion !!}</td>
                        <td>
                            <div class="btn-group" role="group" aria-label="First group">
                                <a href="{{route('clients.edit', $value)}}" class="btn btn-warning"  title="" ><i class="fa fa-edit"></i></a>
                                
                                <form method="POST" action="{{ route('clients.destroy',$value) }}">
                                     @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button type="submit" class="btn btn-info"><i class="fa fa-trash"></i></button>
                                </form>
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr class=" text-center"><td colspan="5"><i class="fa fa-times-circle"></i>&nbsp;No se Encontraron resultados</td></tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="row   d-block mx-auto text-center ">
                    @if($clients)
                    <div class="col-md-12 d-block mx-auto text-center centerpagination">
                        @if(empty($search))
                        {{ $clients->appends([])->links() }}
                        @else
                        {{ $clients->appends(['search'=>$search])->links() }}
                        @endif
                        @if($clients->total()>0)
                        &nbsp;Registros&nbsp;<strong>{{$clients->count()}}</strong>&nbsp;de&nbsp;<strong>{{$clients->total()}}</strong>&nbsp;-&nbsp;Página&nbsp;<strong>{{$clients->currentPage()}}</strong>&nbsp;de&nbsp;<strong>{{$clients->lastPage()}}</strong>
                    </div>
                    @endif
                    @endif
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
        </div>
    </div>
</div>
<!--End::Row-->
@endsection