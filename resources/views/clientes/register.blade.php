@extends('adminlte::page')
@section('content')
<!--Begin::Row-->
<div class="row">
	<div class="col-xl-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Editar Cliente</h3>
            <a href="{{ route('clients.index') }}" class="btn btn-success float-right">
                Clientes &nbsp;
                <i class="fa fa-list-alt"></i>
            </a>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @if(empty($cliente))
        <form class="kt-form" action="{{ route('clients.store') }}" method="POST">
            @else
            <form method="POST" action="{{ route('clients.update',$cliente) }}">
                <input name="_method" type="hidden" value="PUT">
                @endif
                @csrf
                <div class="card-body">
                   @include('flash_message')
                   <div class="form-group">
                    <label class="">Correo Electrónico</label>
                    <input type="email" class="form-control" aria-describedby="emailHelp" autocomplete="off" placeholder="Ingrese correo electrónico" name="email" id="email" value="{{ empty($cliente)?'':$cliente->email }}">
                    <span class="form-text text-muted">Este correo no puede repetirse.</span>
                    @error('email')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="">Nombre</label>
                    <input type="text" class="form-control" aria-describedby="nombreHelp"  autocomplete="off" placeholder="Ingrese Nombre" name="nombre" id="nombre" value="{{ empty($cliente)?'':$cliente->nombre }}">
                    <span class="form-text text-muted">Nombre del Cliente.</span>
                    @error('nombre')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="">Documento</label>
                    <input type="text" class="form-control" aria-describedby="documentoHelp"  autocomplete="off" placeholder="Ingrese Documento" name="documento" id="documento" value="{{ empty($cliente)?'':$cliente->documento }}">
                    <span class="form-text text-muted">Documento del Cliente.</span>
                    @error('documento')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="">Direccion</label>
                    <textarea class="form-control" cols="20" rows="10" name='direccion' id='direccion'>{{ empty($cliente)?'':$cliente->direccion }}</textarea>
                    
                    <span class="form-text text-muted">Direccion del Cliente.</span>
                    @error('direccion')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <span class="kt-margin-left-10">o <a href="{{ route('clients.index') }}" class="kt-link kt-font-bold">Cancelar</a></span>
              </div>
          </div>
      </form>
  </div>
</div>
<!--End::Row-->
@endsection