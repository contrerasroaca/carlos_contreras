<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => 'sentinel:admin,vendedor'], function () {
    Route::get('dashboard', 'HomeController@index')->name('dashboard');
    /**
    * Rutas de usuario
    */    
    Route::group(['middleware' => 'sentinel:admin'], function () {
        Route::resource('users', 'AuthController');
        Route::get('usersactivate/{id}/{estado}', 'AuthController@activate')->name('usersactivate');
        Route::any('userssearch', 'AuthController@search')->name('userssearch');
    });
    Route::any('users_update/{user}', 'AuthController@show')->name('users_update');
    /**
    * Actualizar los datos de usuario por el propio usuario(Nombre, Apellido y Correo)
    */  
    Route::put('usersupdate/{id}', 'AuthController@update_user')->name('usersupdate');

    Route::resource('clients', 'ClienteController');
    Route::any('clientssearch', 'ClienteController@search')->name('clientssearch');
    
});
/*
*Rutas Publicas
*/
Route::post('sendlogin', 'AuthController@login')->name('sendlogin');
Route::get('userslogout', 'AuthController@logout')->name('userslogout');
Route::get('/', function () {
    //Sentinel::registerAndActivate(['email'=>'car@admin.com','password'=>'1234']);
    return view('vendor.adminlte.login');
})->name("/");